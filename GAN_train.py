from __future__ import print_function

import os
import time

import pickle
import lasagne.layers as L
import lasagne.nonlinearities as NL
import lasagne.objectives as O
import lasagne.updates as U
import theano.tensor as T
import theano
from utilities.log import log

import numpy as np
import scipy.misc as misc

import models.generative_network as gen_net
import models.dense_dis_net as dis_net

import sys

DAT = 0

datasets = ["CIFAR","mnist"]
dname = datasets[DAT]

if len(sys.argv) > 1:
    tag = "_" + sys.argv[1]
else:
    tag = "_TEST"

def load_mnist():
   # We first define a download function, supporting both Python 2 and 3.
   if sys.version_info[0] == 2:
       from urllib import urlretrieve
   else:
       from urllib.request import urlretrieve

   def download(filename, source='http://yann.lecun.com/exdb/mnist/'):
       print("Downloading %s" % filename)
       urlretrieve(source + filename, "./data/"+ filename)

   # We then define functions for loading MNIST images and labels.
   # For convenience, they also download the requested files if needed.
   import gzip

   def load_mnist_images(filename):
       if not os.path.exists("./data/"+filename):
           download(filename)
       # Read the inputs in Yann LeCun's binary format.
       with gzip.open("./data/"+filename, 'rb') as f:
           data = np.frombuffer(f.read(), np.uint8, offset=16)
       # The inputs are vectors now, we reshape them to monochrome 2D images,
       # following the shape convention: (examples, channels, rows, columns)
       data = data.reshape(-1, 1, 28, 28)
       # The inputs come as bytes, we convert them to float32 in range [0,1].
       # (Actually to range [0, 255/256], for compatibility to the version
       # provided at http://deeplearning.net/data/mnist/mnist.pkl.gz.)
       return data / np.float32(256)

   def load_mnist_labels(filename):
       if not os.path.exists("./data/"+filename):
           download(filename)
       # Read the labels in Yann LeCun's binary format.
       with gzip.open("./data/"+filename, 'rb') as f:
           data = np.frombuffer(f.read(), np.uint8, offset=8)
       # The labels are vectors of integers now, that's exactly what we want.
       return data

   # We can now download and read the training and test set images and labels.
   X_train = load_mnist_images('train-images-idx3-ubyte.gz')
   y_train = load_mnist_labels('train-labels-idx1-ubyte.gz')
   X_test = load_mnist_images('t10k-images-idx3-ubyte.gz')
   y_test = load_mnist_labels('t10k-labels-idx1-ubyte.gz')

   # We reserve the last 10000 training examples for validation.
   X_train, X_val = X_train[:-10000], X_train[-10000:]
   y_train, y_val = y_train[:-10000], y_train[-10000:]

   # We just return all the arrays in order, as expected in main().
   # (It doesn't matter how we do this as long as we can read them again.)
   return X_train, y_train, X_val, y_val, X_test, y_test


def unpickle(file):
    import cPickle
    fo = open(file, 'rb')
    dict = cPickle.load(fo)
    fo.close()
    return dict

def main():

    start = 0

    LOG = log("logfiles/logfile" + tag)
    ELOG = log("error_logs/error_log" + tag,"genloss,disloss")
    
    if DAT == 0:
        CIFAR = unpickle("./../../data/CIFAR/cifar-100-python/train")
        CIFAR2 = unpickle("./../../data/CIFAR/cifar-100-python/test")
        
        images = np.concatenate([CIFAR["data"],CIFAR2["data"]])
        images = np.reshape(images,(-1,3,32,32)).astype("float32")/255
        images = (images - .5)*2

    else:
        X_train, y_train, X_val, y_val, X_test, y_test = load_mnist()

        images = np.reshape(np.concatenate([X_train,X_test,X_val]),[-1,1,28,28])

    LOG.log(images.shape)
    if DAT == 1:
        misc.imsave(dname+"test.png",np.reshape(images[0],[28,28]))
    else:
        misc.imsave(dname+"test.png",images[0].transpose([1,2,0]))
        
    LOG.log("Image Max: {}".format(np.max(images)))
    LOG.log("Image Min: {}".format(np.min(images)))

    # build variables and networks
    LOG.log("Building Network")

    randin = T.matrix("input","float32")
    image = T.tensor4("image","float32")
    gradin = T.tensor4("image","float32")
    target = T.matrix("target","float32")

    batch = 200

    GEN = gen_net.build(randin,batch,DAT)
    DIS,layers = dis_net.build(image,batch,DAT)
    DIS_grad,layers = dis_net.build(gradin,batch,DAT,LL = layers)

    discrim = L.get_output(DIS)
    generated_image = L.get_output(GEN,deterministic = False)
    output_image = L.get_output(GEN,deterministic = True)

    DIStrain,layers = dis_net.build(generated_image,batch,DAT,LL=layers)
    gen_disc = L.get_output(DIStrain)

    gen_func = theano.function([randin],generated_image,allow_input_downcast = True)
    out_func = theano.function([randin],output_image,allow_input_downcast = True)

    start = 0

    LOG.log("Building Functions")
    #build gen and disc funcs

    gen_f = theano.function([randin],generated_image,allow_input_downcast = True)
    dis_f = theano.function([image],discrim,allow_input_downcast = True)

    #build loss function

    GENloss = - gen_disc.mean()
    #the .9/.1 terms are one-sided label smoothing
    grad_out = L.get_output(DIS_grad)

    Dgrad = T.sqrt((T.jacobian(T.reshape(grad_out,[-1]),gradin)**2).sum(axis = [1,2,3,4]))
    lam = 10

    DISloss = - (discrim.mean() - gen_disc.mean()) + lam * ((Dgrad - 1)**2).mean()

    # build updates
 
    GENupdate = U.adam(GENloss,L.get_all_params(GEN),learning_rate = .001)
    DISupdate = U.adam(DISloss,L.get_all_params(DIS),learning_rate = .001)

    # train

    train_GEN_f = theano.function([randin],GENloss,updates = GENupdate,allow_input_downcast = True)
    train_DIS_f = theano.function([image,randin,gradin],DISloss,updates = DISupdate,allow_input_downcast = True)

    def generate_images(nim,FILE):
        inputs = np.random.normal(0,1,(nim,100))

        out = (out_func(inputs)+1.)/2

        for k in range(len(out)):
            misc.imsave("test_images/"+FILE+"_{}.png".format(k),np.squeeze(out[k].transpose([1,2,0])))  
    

    #NOW WE TRAIN
    BS = batch
    dbs = 10
    pre = 1
    TOT = 20000
    ncrit = 5
    LOG.log("Training on {} samples per epoch".format(TOT))
    for epoch in range(200):
        LOG.log("Starting Epoch {}".format(epoch))
        np.random.shuffle(images)
        GL = []
        DL = []
        for b in range(0,TOT - BS + 1,BS):

            per = round(100*float(b + BS)/TOT,2)

            for k in range(ncrit):
                imbatch = images[b:b+BS]
                rbatch = np.random.normal(0,1,[BS,100])
                eps = np.random.rand(BS,1,1,1)

                GIM = gen_func(rbatch)
 
                disloss = train_DIS_f(imbatch,rbatch,eps*GIM + (1- eps)*imbatch)
                DL.append(disloss)

            if epoch >= pre:
                genloss = train_GEN_f(rbatch)
                GL.append(genloss)
            
                LOG.log("Batch : {}%\t: gloss - {}\tdloss - {}".format(per,
                                                                       round(genloss,3),
                                                                       round(disloss,3)))
            
                ELOG.log("{},{}".format(genloss,disloss),PRINT = False)

            else:
                LOG.log("Batch : {}%\t: dloss - {}".format(per,
                                                           round(disloss,3)))

        if epoch >= pre:
            LOG.log("Epoch {} Finished:\n\tgloss {}\n\tdloss {}".format(epoch,
                                                                        round(np.mean(GL),3),
                                                                        round(np.mean(DL),3)))
        else:
            LOG.log("Epoch {} Finished:\n\tdloss {}".format(epoch,round(np.mean(DL),3)))
            
        if (epoch)%10 == 0:
            FF = open("parameters/"+dname+"_GEN_{}_dense.data".format(epoch + start) + tag,"w")
            pickle.dump(L.get_all_param_values(GEN),FF)
            FF.close()

            FF = open("parameters/"+dname+"_DIS_{}_dense.data".format(epoch + start) + tag,"w")
            pickle.dump(L.get_all_param_values(DIS),FF)
            FF.close()
            
            generate_images(10,dname+"_test_epoch_{}_dense".format(epoch + start) + tag)


if __name__ == "__main__":
    main()
