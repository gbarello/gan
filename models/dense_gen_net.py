import lasagne.layers as L
import theano.tensor as T
import lasagne.nonlinearities as NL

def build(IN,BS):
    INPUT = L.InputLayer(shape = [BS, 100],input_var = IN)

    net = L.DenseLayer(INPUT,1024,nonlinearity = NL.leaky_rectify)
    net = L.DropoutLayer(net)

    net = L.DenseLayer(net,1024,nonlinearity = NL.leaky_rectify)
    net = L.DropoutLayer(net)

    net = L.DenseLayer(net,1024,nonlinearity = NL.leaky_rectify)
    net = L.DropoutLayer(net)

    net = L.DenseLayer(net,3*1024,nonlinearity = NL.tanh)
    #here the size is (32,32)

    net = L.ReshapeLayer(net,[-1,3,32,32])
#    net = L.Conv2DLayer(net,64,3,pad = 'same',nonlinearity = NL.leaky_rectify)
#    net = L.Conv2DLayer(net,3,5,pad = 'same',nonlinearity = NL.tanh)

    return net
