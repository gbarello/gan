import lasagne.layers as L
import lasagne.nonlinearities as NL
import theano.tensor as T

def WGnon(x):
    return x

def build(IN,BS,LL = -1):

    INPUT = L.InputLayer(shape = [BS,3,32,32],input_var = IN)

    CD = cdiff_layer(INPUT)

    INPUT = L.ConcatLayer([INPUT,CD],axis = 1)

    if LL == -1:
        print("Making dense model")

        print(INPUT.output_shape)

        l1 = L.Conv2DLayer(INPUT,32,3,pad = 'same',nonlinearity = NL.leaky_rectify)
        l2 = L.Conv2DLayer(l1,32,3,pad = 'same',nonlinearity = NL.leaky_rectify)

        pool = L.MaxPool2DLayer(l2,2)
        #[16,16]

        print(pool.output_shape)

        l3 = L.Conv2DLayer(pool,128,3,pad = 'same',nonlinearity = NL.leaky_rectify)
        l4 = L.Conv2DLayer(l3,128,3,pad = 'same',nonlinearity = NL.leaky_rectify)

        pool = L.MaxPool2DLayer(l4,2)
        #[8,8]
        print(pool.output_shape)

        l5 = L.Conv2DLayer(pool,256,3,pad = 'same',nonlinearity = NL.leaky_rectify)
        l6 = L.Conv2DLayer(l5,256,3,pad = 'same',nonlinearity = NL.leaky_rectify)

        pool = L.MaxPool2DLayer(l6,2)
        #[4,4]
        print(pool.output_shape)
        
        l7 = L.DenseLayer(pool,512,nonlinearity = NL.leaky_rectify)

        diff = diff_layer(l7)
        con = L.ConcatLayer([l7,diff],axis = 1)

        l8 = L.DenseLayer(con,128,nonlinearity = NL.leaky_rectify)

        l9 = L.DenseLayer(l8,64,nonlinearity = NL.leaky_rectify)

        l10 = L.DenseLayer(l9,1,nonlinearity = WGnon)
        print(l10.output_shape)

    else:
        l1 = L.Conv2DLayer(INPUT,32,3,pad = 'same',nonlinearity = NL.leaky_rectify,
                           W = LL[0].W,
                           b = LL[0].b)
        l2 = L.Conv2DLayer(l1,32,3,pad = 'same',nonlinearity = NL.leaky_rectify,
                           W = LL[1].W,
                           b = LL[1].b)

        pool = L.MaxPool2DLayer(l2,2)
        #[16,16]

        l3 = L.Conv2DLayer(pool,128,3,pad = 'same',nonlinearity = NL.leaky_rectify,
                           W = LL[2].W,
                           b = LL[2].b)
        l4 = L.Conv2DLayer(l3,128,3,pad = 'same',nonlinearity = NL.leaky_rectify,
                           W = LL[3].W,
                           b = LL[3].b)

        pool = L.MaxPool2DLayer(l4,2)
        #[8,8]

        l5 = L.Conv2DLayer(pool,256,3,pad = 'same',nonlinearity = NL.leaky_rectify,
                           W = LL[4].W,
                           b = LL[4].b)
        l6 = L.Conv2DLayer(l5,256,3,pad = 'same',nonlinearity = NL.leaky_rectify,
                           W = LL[5].W,
                           b = LL[5].b)

        pool = L.MaxPool2DLayer(l6,2)
        #[4,4]
        
        l7 = L.DenseLayer(pool,512,nonlinearity = NL.leaky_rectify,
                           W = LL[6].W,
                           b = LL[6].b)


        diff = diff_layer(l7)
        con = L.ConcatLayer([l7,diff],axis = 1)

        l8 = L.DenseLayer(con,128,nonlinearity = NL.leaky_rectify,
                           W = LL[7].W,
                           b = LL[7].b)

        l9 = L.DenseLayer(l8,64,nonlinearity = NL.leaky_rectify,
                           W = LL[8].W,
                           b = LL[8].b)

        l10 = L.DenseLayer(l9,1,nonlinearity = WGnon,
                           W = LL[9].W,
                           b = LL[9].b)

    return l10,[l1,l2,l3,l4,l5,l6,l7,l8,l9,l10]

class diff_layer(L.Layer):
    def get_output_for(self,input,**kwargs):
        #for each entry I want to return the sum of exp of differences between it and each other

        temp = input
        two = T.tile(temp,[temp.shape[0],1])
        two = T.reshape(two,(temp.shape[0],temp.shape[0],-1))
        two = T.transpose(two,(1,0,2))
        
        ab_diff = (temp.dimshuffle([0,'x',1]) - two)**2

        ab_out = T.mean(T.abs_(ab_diff),axis = 1,keepdims = True)

        return T.exp(-ab_out)
        
class diff_layer(L.Layer):
    def get_output_for(self,input,**kwargs):
        #for each entry I want to return the sum of exp of differences between it and each other

        ish = input.shape

        temp = input
        two = T.tile(temp,[ish[0],1])
        two = T.reshape(two,(ish[0],ish[0],ish[1]))
        two = T.transpose(two,(1,0,2))

        re_in = T.reshape(temp,(ish[0],1,ish[1]))

        ab_diff = ((re_in - two)**2).mean(axis = 1)

        return T.exp(-ab_diff)

class cdiff_layer(L.Layer):
    def get_output_for(self,input,**kwargs):
        #for each entry I want to return the sum of exp of differences between it and each other

        ish = input.shape

        temp = input
        two = T.tile(temp,[ish[0]] + [1 for k in ish[1:]])
        two = T.reshape(two,(ish[0],ish[0]) + tuple(ish[1:]))
        two = two.swapaxes(0,1)

        re_in = T.reshape(temp,(ish[0],1) + tuple(ish[1:]))

        ab_diff = ((re_in - two)**2).mean(axis = 1)

        return T.exp(-ab_diff)
