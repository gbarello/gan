import lasagne.layers as L
import lasagne.nonlinearities as NL
import theano.tensor as T

def build(IN,LL = -1):

    INPUT = L.InputLayer(shape = [None, 3,32,32],input_var = IN)

    if LL == -1:
        l1 = L.Conv2DLayer(INPUT,128,3,pad = 'same',nonlinearity = NL.leaky_rectify)
        l2 = L.Conv2DLayer(l1,256,3,pad = 'same',nonlinearity = NL.leaky_rectify)
        l3 = L.Conv2DLayer(l2,256,3,pad = 'same',nonlinearity = NL.leaky_rectify)
        l4 = L.Conv2DLayer(l3,256,3,pad = 'same',nonlinearity = NL.leaky_rectify)
        
        l5 = L.DenseLayer(l4,100,nonlinearity = NL.leaky_rectify)

        l6 = L.DenseLayer(l5,1,nonlinearity = NL.linear)

    else:
        l1 = L.Conv2DLayer(INPUT,128,3,pad = 'same',W = LL[0].W,b = LL[0].b,nonlinearity = NL.leaky_rectify)
        l2 = L.Conv2DLayer(l1,256,3,pad = 'same',W = LL[1].W,b = LL[1].b,nonlinearity = NL.leaky_rectify)
        l3 = L.Conv2DLayer(l2,256,3,pad = 'same',W = LL[2].W,b = LL[2].b,nonlinearity = NL.leaky_rectify)
        l4 = L.Conv2DLayer(l3,256,3,pad = 'same',W = LL[3].W,b = LL[3].b,nonlinearity = NL.leaky_rectify)
        
        l5 = L.DenseLayer(l4,100,W = LL[4].W,b = LL[4].b,nonlinearity = NL.leaky_rectify)

        l6 = L.DenseLayer(l5,1,nonlinearity = NL.linear,W = LL[5].W,b = LL[5].b)

    return l6,[l1,l2,l3,l4,l5,l6]
