import lasagne.layers as L
import theano.tensor as T
import lasagne.nonlinearities as NL

def build(IN,BS):
    INPUT = L.InputLayer(shape = [BS, 100],input_var = IN)

    ss = 4
    ff = 256

    net = L.DenseLayer(INPUT,ff*ss*ss,nonlinearity = NL.leaky_rectify)

    net = L.ReshapeLayer(net,(-1,ff,ss,ss))
    print(net.output_shape)

    net = L.TransposedConv2DLayer(net,128,3,stride = 2,crop = 1,nonlinearity = NL.leaky_rectify)
    print(net.output_shape)

    net = L.TransposedConv2DLayer(net,64,3,stride = 2,crop = 1,nonlinearity = NL.leaky_rectify)
    print(net.output_shape)

    net = L.TransposedConv2DLayer(net,32,4,stride = 2,nonlinearity = NL.leaky_rectify)
    print(net.output_shape)

    net = L.TransposedConv2DLayer(net,16,3,nonlinearity = NL.leaky_rectify)
    print(net.output_shape)

    net = L.TransposedConv2DLayer(net,3,3,nonlinearity = NL.leaky_rectify)
    print(net.output_shape)

    return net
