import pickle
import lasagne.layers as L
import lasagne.nonlinearities as NL
import lasagne.objectives as O
import lasagne.updates as U
import theano.tensor as T
import theano
from utilities.log import log

import numpy as np
import scipy.misc as misc

import models.generative_network as gen_net
import models.dense_dis_net as dis_net

import sys


def unpickle(file):
    import cPickle
    fo = open(file, 'rb')
    dict = cPickle.load(fo)
    fo.close()
    return dict

def main():

    if len(sys.argv) < 3:
        print("Must include at least 2 command line arguments!\n\t[1] - name\n\t[2] - epoch")
        exit()

    tag = "_" + sys.argv[1]
    epoch = int(sys.argv[2])

    LOG = log("logfiles/compare_logfile" + tag)
    
    if 0:
        CIFAR = unpickle("./../../data/CIFAR/cifar-100-python/train")
        CIFAR2 = unpickle("./../../data/CIFAR/cifar-100-python/test")
        
        images = np.concatenate([CIFAR["data"],CIFAR2["data"]])
        images = np.reshape(images,(-1,3,32,32)).astype("float32")/255
        images = (images - .5)*2

    else:
        CIFAR = [unpickle("./../../data/CIFAR/cifar-10-batches-py/data_batch_{}".format(k))["data"] for k in range(1,6)]  
      
        images = np.concatenate(CIFAR)
        images = np.reshape(images,(-1,3,32,32)).astype("float32")/255
        images = (images - .5)*2
        

    LOG.log(images.shape)
    misc.imsave("test.png",images[0].transpose([1,2,0]))

    LOG.log("Image Max: {}".format(np.max(images)))
    LOG.log("Image Min: {}".format(np.min(images)))

    # build variables and networks
    LOG.log("Building Network")

    randin = T.matrix("input","float32")
    image = T.tensor4("image","float32")
    target = T.matrix("target","float32")

    batch = 200

    GEN = gen_net.build(randin,batch)
    DIS,dis_layers = dis_net.build(image,batch)

    diff_layer = L.get_output(dis_layers[6])
    diff_func = theano.function([image],diff_layer,allow_input_downcast = True)

    discrim = L.get_output(DIS)
    generated_image = L.get_output(GEN,deterministic = False)

    output_image = L.get_output(GEN,deterministic = True)

    DIStrain,dis_layers = dis_net.build(generated_image,batch,LL=dis_layers)
    gen_disc = L.get_output(DIStrain)
    
    gen_func = theano.function([randin],generated_image,allow_input_downcast = True)
    out_func = theano.function([randin],output_image,allow_input_downcast = True)

    #build gen and disc funcs

    gen_f = theano.function([randin],generated_image,allow_input_downcast = True)
    dis_f = theano.function([image],discrim,allow_input_downcast = True)

    ##load parameter files:
    start = 0
    if 1:
        start = 200

        FF = open("parameters/GEN_{}_dense.data".format(start - 1) + tag,"r")
        L.set_all_param_values(GEN,pickle.load(FF))
        FF.close()
        
        FF = open("parameters/DIS_{}_dense.data".format(start - 1) + tag,"r")
        L.set_all_param_values(DIS,pickle.load(FF))
        FF.close()

    LOG.log("Building Functions")

    def generate_images(nim,FILE):
        inputs = np.random.normal(0,1,(nim,100))

        out = (out_func(inputs)+1.)/2

        for k in range(len(out)):
            misc.imsave("test_images/"+FILE+"_{}.png".format(k),out[k].transpose([1,2,0]))        
    

    if 0:
    #I want to generate a bunch of images, and then save them along with their few nearest neighbors (in terms of the diff layer features)
        
        LOG.log("computing features")
        
        BS = batch
        
        outs = []
        
        for bb in range(0,len(images),BS):
            print(round(100*float(bb+BS)/(len(images)),2))
            outs.append(diff_func(images[bb:bb+BS]))
            
        data_diffs = np.concatenate(outs)
        
        imseeds = np.random.normal(0,1,(BS,100))
        
        fake_ims = out_func(imseeds)
        fake_diffs = diff_func(fake_ims)
        
        ind = []
        
        F = open("test_compare_images/test_compare_images_{}.csv".format(epoch),"w")
        F.write("image_index,0,1,2,3,4,5,6,7,8,9,10\n")

        for k in range(len(fake_diffs)):
            print(k)
            temp_diffs = np.sum((data_diffs - np.array([fake_diffs[k]]))**2,axis = 1)
            
            ind = np.argsort(temp_diffs)
            
            misc.imsave("test_compare_images/test_compare_{}_{}_fake.png".format(k,epoch),((fake_ims[k] + 1)/2).transpose([1,2,0]))
            
            pr = str(k)
            
            for j in range(10):
                pr += ",{}".format(temp_diffs[ind[j]])
            
                misc.imsave("test_compare_images/test_compare_{}_{}_true_{}.png".format(k,epoch,j),((images[ind[j]] + 1)/2).transpose([1,2,0]))

            F.write(pr+"\n")

        F.close()

    else:
    #I want to generate a bunch of images, and then save them along with their few nearest neighbors (in terms of pixel level features)
        
        LOG.log("computing features")
        BS = batch

        imseeds = np.random.normal(0,1,(BS,100))
        
        fake_ims = out_func(imseeds)
        
        ind = []
        
        F = open("test_compare_images/test_compare_images_px_{}.csv".format(epoch),"w")
        F.write("image_index,0,1,2,3,4,5,6,7,8,9,10\n")

        for k in range(len(fake_ims)):
            print(k)
            temp_diffs = np.sum((images - np.array([fake_ims[k]]))**2,axis = (1,2,3))
            
            ind = np.argsort(temp_diffs)
            
            misc.imsave("test_compare_images/test_compare_{}_{}_px_fake.png".format(k,epoch),((fake_ims[k] + 1)/2).transpose([1,2,0]))
            
            pr = str(k)
            
            for j in range(10):
                pr += ",{}".format(temp_diffs[ind[j]])
            
                misc.imsave("test_compare_images/test_compare_{}_{}_px_true_{}.png".format(k,epoch,j),((images[ind[j]] + 1)/2).transpose([1,2,0]))

            F.write(pr+"\n")

        F.close()
        
        

if __name__ == "__main__":
    main()
