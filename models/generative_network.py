import lasagne.layers as L
import theano.tensor as T
import lasagne.nonlinearities as NL

def build(IN,BS,data):
    INPUT = L.InputLayer(shape = [BS, 100],input_var = IN)

    ss = 3
    ff = 2*1024

    net = L.DenseLayer(INPUT,ff*ss*ss,nonlinearity = NL.leaky_rectify)

    net = L.ReshapeLayer(net,(-1,ff,ss,ss))
    print(net.output_shape)
    
    if data == 0:
        net = L.TransposedConv2DLayer(net,512,3,stride = 2,nonlinearity = NL.leaky_rectify)
        print(net.output_shape)
    else:
        net = L.TransposedConv2DLayer(net,512,4,stride = 1,nonlinearity = NL.leaky_rectify)
        print("AAA",net.output_shape)

    net = L.TransposedConv2DLayer(net,256,3,stride = 2,nonlinearity = NL.leaky_rectify)
    print(net.output_shape)

    net = L.TransposedConv2DLayer(net,128,5,stride = 2,nonlinearity = NL.leaky_rectify)
    print(net.output_shape)

    net = L.TransposedConv2DLayer(net,64,3,nonlinearity = NL.leaky_rectify)
    print(net.output_shape)

    net = L.Conv2DLayer(net,32,4,nonlinearity = NL.leaky_rectify)
    print(net.output_shape)

    net = L.Conv2DLayer(net,16,5,pad = 'same',nonlinearity = NL.leaky_rectify)
    print(net.output_shape)

    net = L.Conv2DLayer(net,8,5,pad = 'same',nonlinearity = NL.leaky_rectify)
    print(net.output_shape)

    if data == 1:
        net = L.Conv2DLayer(net,1,5,pad = 'same',nonlinearity = NL.tanh)
        print(net.output_shape)
    else:
        net = L.Conv2DLayer(net,3,5,pad = 'same',nonlinearity = NL.tanh)
        print(net.output_shape)

    return net
