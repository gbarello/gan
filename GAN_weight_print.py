import pickle
import lasagne.layers as L
import lasagne.nonlinearities as NL
import lasagne.objectives as O
import lasagne.updates as U
import theano.tensor as T
import theano
from utilities.log import log

import numpy as np
import scipy.misc as misc

import models.generative_network as gen_net
import models.dense_dis_net as dis_net

import sys


def unpickle(file):
    import cPickle
    fo = open(file, 'rb')
    dict = cPickle.load(fo)
    fo.close()
    return dict

def main():

    if len(sys.argv) < 3:
        print("Must include at least 2 command line arguments!\n\t[1] - name\n\t[2] - epoch")
        exit()

    tag = "_" + sys.argv[1]
    epoch = int(sys.argv[2])

    LOG = log("logfiles/weight_logfile" + tag)
    
    # build variables and networks
    LOG.log("Building Network")

    randin = T.matrix("input","float32")
    image = T.tensor4("image","float32")
    target = T.matrix("target","float32")

    batch = 200

    GEN = gen_net.build(randin,batch)
    DIS,dis_layers = dis_net.build(image,batch)

    diff_layer = L.get_output(dis_layers[6])
    diff_func = theano.function([image],diff_layer,allow_input_downcast = True)

    discrim = L.get_output(DIS)
    generated_image = L.get_output(GEN,deterministic = False)

    output_image = L.get_output(GEN,deterministic = True)

    DIStrain,dis_layers = dis_net.build(generated_image,batch,LL=dis_layers)
    gen_disc = L.get_output(DIStrain)
    
    gen_func = theano.function([randin],generated_image,allow_input_downcast = True)
    out_func = theano.function([randin],output_image,allow_input_downcast = True)

    #build gen and disc funcs

    gen_f = theano.function([randin],generated_image,allow_input_downcast = True)
    dis_f = theano.function([image],discrim,allow_input_downcast = True)

    ##load parameter files:
    start = 0
    if 1:
        start = 200

        FF = open("parameters/GEN_{}_dense.data".format(start - 1) + tag,"r")
        L.set_all_param_values(GEN,pickle.load(FF))
        FF.close()
        
        FF = open("parameters/DIS_{}_dense.data".format(start - 1) + tag,"r")
        L.set_all_param_values(DIS,pickle.load(FF))
        FF.close()

    LOG.log("done grabbing params")
    ll = 0

    for p in L.get_all_param_values(GEN):
        print(p.shape)

        if len(p.shape) == 4:
            g = np.reshape(p,[p.shape[0]*p.shape[1],-1])
            np.savetxt("./weights/GEN{}_{}_{}_{}.csv".format(tag,epoch,ll,p.shape),g)
            ll += 1

    LOG.log("dis")

    ll = 0
    for p in L.get_all_param_values(DIS):
        print(p.shape)

        if len(p.shape) == 4:
            g = np.reshape(p,[p.shape[0]*p.shape[1],-1])
            np.savetxt("./weights/DIS{}_{}_{}_{}.csv".format(tag,epoch,ll,p.shape),g)
            ll += 1


if __name__ == "__main__":
    main()
